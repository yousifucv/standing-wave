
long currentTime = 0;
long prevTime = 0;
long dt = 0;

PVector[] points;
float[] pointTimes;
float pointTimeDelta;//how far are the temporal points away from each other
float pointPlayIncrement; //how much to increment their temporal values per frame
int numOfPoints;

void setup() {
  size(600, 600, P2D);
  frameRate(60);
  numOfPoints = 200;
  pointTimeDelta = 0.05f;
  pointPlayIncrement = 0.016f;
  points = new PVector[numOfPoints];
  pointTimes = new float[numOfPoints];
  float time = 0f;
  for (int i = 0; i < pointTimes.length; i++) {
    pointTimes[i] = time;
    time += pointPlayIncrement;
  }
  float xOffset = 0f;
  for (int i = 0; i < pointTimes.length; i++) {
    points[i] = new PVector(20 + xOffset, height/2);
    xOffset += ((width - 50) * 1.0f / points.length);
  }
}

//map(mouseX, 0, width, 0, 1000); 

Wave wave1 = new Wave(200, 4 * PI); 
Wave wave2 = new Wave(100, 2 * PI); 


void draw() {
  background(0);
  prevTime = currentTime;
  currentTime = millis();
  dt = currentTime - prevTime;
  
  wave2 = new Wave(100, map(mouseX, 0, width, 0, 2) * PI); 

  stroke(255);
  strokeWeight(3);
  for (int i = 0; i < points.length; i++) {
    point(points[i].x, points[i].y + wave1.f(pointTimes[i]) + wave2.f(pointTimes[i]));
    //pointTimes[i] += pointPlayIncrement;
  }

  if (isRecording)
    saveFrame("frames/####.png");
}

boolean isRecording = false; 
void keyPressed() {
  if (keyCode == ENTER) {
    isRecording = !isRecording;
  }
  if (isRecording) { 
    println("Recording");
  } else { 
    println("Stopped");
  }
}


class Wave {

  float amplitude;
  float omega; 

  Wave(float amplitude, float omega) {
    this.amplitude = amplitude;
    this.omega = omega;
  }

  float f(float t_secs) {
    return amplitude * sin(omega * t_secs);
  }
}

